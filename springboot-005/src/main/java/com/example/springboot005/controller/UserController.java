package com.example.springboot005.controller;

import com.example.springboot005.entity.User;
import com.example.springboot005.respository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@Controller
public class UserController {
    @Autowired //Di
    UserRepository userRepository; // CRUD

    @RequestMapping(path = "/")
    public String index(Model model){
        List<User> userList = (List<User>) userRepository.findAll();
        //step 3 return data to view
        model.addAttribute("users",userList);
        return "index";
    }

    @RequestMapping(value = "/add")
    public String addUser(Model model){
        model.addAttribute("users1",new User());
        return "addUser";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveUser(User user){
        userRepository.save(user);
        return "redirect:/";
    }
    @RequestMapping(value = "/search")
    public String searchByName(@RequestParam("name") String userName,Model model){
        model.addAttribute("search",userRepository.findByname(userName));
        return "index";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editUser(@RequestParam("id") Long userId, Model model){
        Optional<User> userOptional = userRepository.findById(userId);
        userOptional.ifPresent(user -> model.addAttribute("user",user));
        return "editUser";
    }
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") Long userId, Model model){
      userRepository.deleteById(userId);
      return "redirect:/";
    }


}
