package com.example.springboot005.respository;

import com.example.springboot005.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,Long> {
    Iterable<User> findByname(String name);
}
